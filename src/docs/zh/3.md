# 3. 状态变化清除 / ステータス変化リセット

![](../merge/BlendBuff-Clear.png) 

**`StatusChangeReset`、状态重置。**清除某个状态上的状态变化。若作用在某个状态上，则清除其全部的 Buff 和 Debuff。
