# 8. 元素耐性 / 属性耐性

![](../merge/Buff-Fire.png) ![](../merge/Buff-Water.png) ![](../merge/Buff-Earth.png) ![](../merge/Buff-Wind.png) ![](../merge/Buff-Moon.png) ![](../merge/Buff-Sun.png)

![](../merge/Debuff-Fire.png) ![](../merge/Debuff-Water.png) ![](../merge/Debuff-Earth.png) ![](../merge/Debuff-Wind.png) ![](../merge/Debuff-Moon.png) ![](../merge/Debuff-Sun.png)

**`ElementResist`、耐性。**使自身对于某种元素的耐性发生变化。作用在元素克制关系上。

## 计算方式

蹩脚的定义：元素耐性针对的对象都是被攻击者。元素耐性数值越高，受到该元素伤害会越大。

$$
元素耐性 = \begin{cases}
[0.5 \times (1 - 元素耐性变化 - 元素耐性建筑加成)] ^{0.9} _{0.1}, & 当对该元素有耐性 \\\\\\\\
[1.0 \times (1 - 元素耐性变化 - 元素耐性建筑加成)] ^{1.4} _{0.6}, & 当普通 \\\\\\\\
[2.0 \times (1 - 元素耐性变化 - 元素耐性建筑加成)] ^{2.4} _{1.6}, & 当对该元素无耐性
\end{cases}
$$

当元素耐性上升时，元素耐性变化为正数。

元素耐性建筑暂时没有实装，按 0 计算。

注意，由于元素耐性针对的对象都是被攻击者，即玩家角色攻击敌人，带入计算的是敌人的元素耐性。也就是说，若玩家攻击敌人，提升玩家的元素耐性是没有意义的，需要降低敌人的元素耐性才可以造成更大的伤害。

例如，玩家对敌人使用了元素耐性中幅降低（-20%）的技能，玩家对敌人元素克制，那么此时若玩家攻击敌人，元素耐性为：

$$
元素耐性 = 2.0 \times (1 - (-20\\%)) = 2.4
$$

即已经达到上限。

## 持续时间

按照目标自身的行动回合数量计算。多个状态变化之间独立存在，分别计算时间。

## 叠加方式

多个元素耐性变化之间独立存在，作用效果数值相加。

## 数值与技能量描述

下面的表格归纳了角色中该技能的一般数值。不排除有例外的可能性。


|      | 大幅降低 | 中幅降低 | 小幅降低 | 小幅提升 | 中幅提升 | 大幅提升 |
| ---- | -------- | -------- | -------- | -------- | -------- | -------- |
| 耐性 | -30%     | -20%     | -10%     | 10%      | 20%      | 30%      |

该技能数值一般按照第 0 条技能成长曲线成长。
