import Vue from 'vue'
import Router from 'vue-router'

import Home from "./views/Home.vue"
import Damage from "./views/Damage.vue"
import Order from "./views/Order.vue"
import Probability from "./views/Probability.vue"
import Status from "./views/Status.vue"
import Document from "./views/Document.vue"

Vue.use(Router)

export default new Router({
    // mode: 'legacy',
    // eslint-disable-next-line
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return {
                selector: to.hash,
                offset: {
                    x: 0,
                    y: 64,
                }
            }
        } else {
            return {
                x: 0,
                y: 0,
            }
        }
    },
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            name: 'Home',
            component: Home
        },
        {
            path: '/damage',
            name: 'Damage',
            component: Damage
        },
        {
            path: '/order',
            name: 'Order',
            component: Order
        },
        {
            path: '/probability',
            name: 'Probability',
            component: Probability
        },
        {
            path: '/status',
            name: 'Status',
            component: Status
        },
        {
            path: '/document',
            name: 'Documents',
            component: Document
        },
        {
            path: '/document/:title',
            name: 'Document',
            component: Document
        }
    ]
})
