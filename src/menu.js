export default [
    {
        name: "Damage", to: "/damage", src: "damage.svg",
        submenu: [
            { name: "Damage Value", to: "#damage" },
            { name: "Stun Gauge", to: "#stun" }
        ]
    },
    {
        name: "Order", to: "/order", src: "order.svg"
    },
    {
        name: "Probability", to: "/probability", src: "probability.svg",
        submenu: [
            { name: "Critical Rate", to: "#critical" },
            { name: "Hate & AI", to: "#hate" },
            { name: "Gacha", to: "#gacha" }
        ]
    },
    {
        name: "Status", to: "/status", src: "status.svg"
    },
    {
        name: "Documents", to: "/document", src: "document.svg",
        submenu: [
            { name: "Chapter 0", to: "/0" },
            { name: "Chapter 1", to: "/1" },
            { name: "Chapter 2", to: "/2" },
            { name: "Chapter 3", to: "/3" },
            { name: "Chapter 4", to: "/4" },
            { name: "Chapter 5", to: "/5" },
            { name: "Chapter 6", to: "/6" },
            { name: "Chapter 7", to: "/7" },
            { name: "Chapter 8", to: "/8" },
            { name: "Chapter 9", to: "/9" },
            { name: "Chapter 10", to: "/10" },
            { name: "Chapter 11", to: "/11" },
            { name: "Chapter 12", to: "/12" },
            { name: "Chapter 13", to: "/13" },
            { name: "Chapter 14", to: "/14" },
            { name: "Chapter 15", to: "/15" },
            { name: "Chapter 16", to: "/16" },
            { name: "Chapter 17", to: "/17" },
            { name: "Chapter 18", to: "/18" },
            { name: "Chapter 19", to: "/19" },
            { name: "Chapter 20", to: "/20" },
            { name: "Chapter 21", to: "/21" },
            { name: "Chapter 22", to: "/22" },
            { name: "Chapter 23", to: "/23" },
            { name: "Chapter 24", to: "/24" },
            { name: "Chapter 25", to: "/25" },
            { name: "Chapter 26", to: "/26" },
            { name: "Appendix 0", to: "/appendix0" },
            { name: "Appendix 1", to: "/appendix1" },
            { name: "Appendix 2", to: "/appendix2" },
            { name: "Appendix 3", to: "/appendix3" },
        ]
    }
]
