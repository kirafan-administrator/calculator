module.exports = {
    publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
    chainWebpack: config => {
        config.module
            .rule('markdown')
            .test(/\.md$/)
            .use('html-loader')
            .loader('html-loader')
            .end()
            .use('markdown-loader')
            .loader('markdown-loader')
            .end()
    },
    runtimeCompiler: true,
}